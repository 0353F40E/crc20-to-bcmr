#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
    uint8_t symbolBytes;
    if (
        argc > 2
        && sscanf(argv[1], "%d", &symbolBytes) == 1
        && symbolBytes > 0
    ) {
        size_t totalBytes = strlen(argv[2]) / 2;
        if (totalBytes < symbolBytes + 1)
            return 1;
        size_t nameBytes = totalBytes - symbolBytes - 1;
        char *metadata = argv[2];
        printf("%d\n", symbolBytes);
        fwrite(metadata, sizeof(char), symbolBytes*2, stdout);
        printf("\n");
        char c = metadata[(symbolBytes+1)*2];
        metadata[(symbolBytes+1)*2] = 0;
        printf("%d\n", strtol(&metadata[symbolBytes*2], NULL, 16));
        metadata[(symbolBytes+1)*2] = c;
        fwrite(&metadata[(symbolBytes+1)*2], sizeof(char), (totalBytes - symbolBytes - 1)*2, stdout);
        printf("\n");
        return 0;
    }
    else {
        return 1;
    }
}
