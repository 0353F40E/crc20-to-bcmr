[
  .data.transaction[] |
  {hash: .hash}
    + .outputs[]
] |
map(
  [
    .hash,
    .output_index,
    .value_satoshis,
    .token_category,
    .nonfungible_token_capability,
    .nonfungible_token_commitment,
    .fungible_token_amount,
    .locking_bytecode
  ] |
  join(",")
) |
join("\n")
