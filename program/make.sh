./make_hash160.sh
gcc parse_pushes.c -o parse_pushes
gcc parse_crc20_metadata.c -o parse_crc20_metadata
./create_database.sh
./import_tables.sh
lastdb=$(sqlite3 crc20.sqlite 'select max(tx_block_height) from genesis_tx_inputs')
echo $lastdb > .block_height
date +"%Y-%m-%dT%H:%M:%S.000Z" -d'@'$(./get_height_timestamp.sh $(cat .block_height)) > .block_timestamp
mkdir -p ../bcmr
jq -s '.[0] * .[1]' <(./generate_registry_header.sh $(cat .block_timestamp)) <(./generate_identities.sh) > ../bcmr/bitcoin-cash-metadata-registry.json
