[
  .data.transaction[] |
  {in_block_height: .block_inclusions[0].block.height}
    + {in_block_timestamp: .block_inclusions[0].block.timestamp}
    + {hash: .hash}
    + {version: .version}
    + {input_count: .input_count}
    + .inputs[]
    + {output_count: .output_count}
    + {locktime: .locktime}
] |
map(
  [
    .in_block_height,
    .in_block_timestamp,
    .hash,
    .version,
    .input_count,
    .outpoint.transaction.block_inclusions[0].block.height,
    .outpoint.transaction.block_inclusions[0].block.timestamp,
    .outpoint.value_satoshis,
    .outpoint.token_category,
    .outpoint.nonfungible_token_capability,
    .outpoint.nonfungible_token_commitment,
    .outpoint.fungible_token_amount,
    .outpoint.locking_bytecode,
    .input_index,
    .outpoint_transaction_hash,
    .outpoint_index,
    .unlocking_bytecode,
    .sequence_number,
    .output_count,
    .locktime
  ] |
  join(",")
) |
join("\n")
