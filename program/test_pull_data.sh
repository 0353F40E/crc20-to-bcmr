# function get_crc20_info()
# args: unlocking_bytecode
# output: csv of CRC-20 metadata
source ./get_crc20_info.sh

query_file="crc20-genesis.graphml"
api_url="https://gql.chaingraph.pat.mn/v1/graphql"
api_origin="https://chaingraph.pat.mn"

query='{"query":'"$(jq -Rsa <$query_file)"'}'
result=$(curl -s \
    "$api_url" \
    -H 'Accept-Encoding: gzip, deflate, br' \
    -H 'Content-Type: application/json' \
    -H 'Accept: application/json' \
    -H 'Connection: keep-alive' \
    -H 'DNT: 1' \
    -H 'Origin: '"$api_origin" \
    --data-binary "$query" \
    --compressed \
)

echo TABLE OUTPUTS
jq -f -r outputs.jq <(echo "$result")
echo TABLE INPUTS
jq -f -r inputs.jq <(echo "$result") \
| sed 's/\,\\x/\,/g' \
| awk  -F "," '{OFS=","; "get_crc20_info " $17 | getline rp1; print $0,rp1}'
