last_scanned=$(cat .block_height)
if [ -z $last_scanned ]
then
    last_scanned=792772
fi
./pull_data.sh $((last_scanned+1)) $(./get_current_height.sh) 1
./export_tables.sh
jq -s '.[0] * .[1]' <(./generate_registry_header.sh $(cat .block_timestamp)) <(./generate_identities.sh) > ../bcmr/bitcoin-cash-metadata-registry.json
