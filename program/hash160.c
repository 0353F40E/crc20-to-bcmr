#include <openssl/ripemd.h>
#include <openssl/sha.h>
#include <stdio.h>

int main(void)
{
    SHA256_CTX handle_sha256;
    uint8_t result_sha256[32];
    int c;
    uint8_t byte;
    SHA256_Init(&handle_sha256);
    while ((c = getchar()) && c != EOF) {
        byte = (uint8_t) c;
        SHA256_Update(&handle_sha256, (uint8_t*) &byte, 1);
    }
    SHA256_Final(result_sha256, &handle_sha256);

    RIPEMD160_CTX handle_ripemd160;
    uint8_t result_ripemd160[20];
    RIPEMD160_Init(&handle_ripemd160);
    RIPEMD160_Update(&handle_ripemd160, (uint8_t*) result_sha256, 32);
    RIPEMD160_Final(result_ripemd160, &handle_ripemd160);
    for (size_t i = 0; i < 20; ++i)
        printf("%02x", result_ripemd160[i]);
    return 0;
}
