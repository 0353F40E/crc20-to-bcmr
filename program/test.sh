result=$(curl -s "https://gql.chaingraph.pat.mn/v1/graphql" --data-binary '{"query":'"$(jq -Rsa <crc20-genesis.graphml)"'}')
echo TABLE INPUTS
jq -f -r inputs.jq <(echo "$result")
echo TABLE OUTPUTS
jq -f -r outputs.jq <(echo "$result")
