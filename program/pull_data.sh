#!/bin/bash

height_start=$1
height_end=$2
query_blocks=$3

# chaingraph query
query_file="crc20-genesis.graphml"

# sqlite database file
database_file_name="crc20.sqlite"

# chaingraph server
api_url="https://gql.chaingraph.pat.mn/v1/graphql"
api_origin="https://chaingraph.pat.mn"

# args: unlocking_bytecode
# function get_crc20_info()
# output: csv of CRC-20 metadata
source ./get_crc20_info.sh

# args: database_file_name, table_name
function csv_to_sqlite() {
  sqlite3 -csv "$1" ".import '|cat -' $2"
}

# args: query, limit, offset, height_gte, height_lt
function update_query_parameters () {
    local q="$1"
    q=$(echo "$q" | sed -r -e 's/limit:\ [0-9]+/limit:\ '$2'/')
    q=$(echo "$q" | sed -r -e 's/offset:\ [0-9]+/offset:\ '$3'/')
    q=$(echo "$q" | sed -r -e 's/_gte:\ [0-9]+/_gte:\ '$4'/')
    q=$(echo "$q" | sed -r -e 's/_lt:\ [0-9]+/_lt:\ '$5'/')
    echo "$q"
}

# format the query
query='{"query":'"$(jq -Rsa <$query_file)"'}'

height=$height_start
while [ $height -le $((height_end-query_blocks+1)) ]
do
    limit=0
    offset=0
    height_gte=$height
    height_lt=$((height+query_blocks))
    echo "query_blocks_range ["$height_gte", "$height_lt">" >> /dev/stderr
    query=$(update_query_parameters "$query" $limit $offset $height_gte $height_lt)
    result=$(curl -s "$api_url" \
    -H 'Accept-Encoding: gzip, deflate, br' \
    -H 'Content-Type: application/json' \
    -H 'Accept: application/json' \
    -H 'Connection: keep-alive' \
    -H 'DNT: 1' \
    -H 'Origin: '"$api_origin" \
    --data-binary "$query" \
    --compressed)
    status=$?
    # echo "$result" >> /dev/stderr
    errors=$(echo "$result" | jq -r '.errors | length')
    if [ -z "$errors" ] && [ "$status" -ne "0" ]; then errors=1; fi
    echo $errors' errors' >> /dev/stderr
    if [ "$errors" -ne "0" ]
    then
        echo "$query" | jq >> /dev/stderr
        echo "$result" | jq >> /dev/stderr
        exit 1
    fi
    rows=$(echo "$result" | jq -r '.data.transaction[] | map(.) | length')
    rowcount=0
    for i in ${rows[@]}; do
        let rowcount+=$i
    done
    echo $rowcount' rows' >> /dev/stderr
    echo $((height_lt-1)) > .block_height
    date +"%Y-%m-%dT%H:%M:%S.000Z" -d'@'$(./get_height_timestamp.sh $((height_lt-1))) > .block_timestamp

    if [ $rowcount -gt 0 ]
    then
        jq -f -r outputs.jq <(echo "$result") \
        | sed 's/\,\\x/\,/g' \
        | sed 's/^\\x//g' \
        | csv_to_sqlite "$database_file_name" "genesis_tx_outputs" 2>&1 | grep -v 'UNIQUE constraint failed' 1>&2

        jq -f -r inputs.jq <(echo "$result") \
        | sed 's/\,\\x/\,/g' \
        | sed 's/^\\x//g' \
        | awk  -F "," '{OFS=","; "get_crc20_info " $17 | getline rp1; print $0,rp1}' \
        | csv_to_sqlite "$database_file_name" "genesis_tx_inputs" 2>&1 | grep -v 'UNIQUE constraint failed' 1>&2
    fi

    height=$((height+query_blocks))
    sleep 1
done

