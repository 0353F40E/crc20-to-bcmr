sqlite3 -csv crc20.sqlite ".import --skip 1 genesis_tx_inputs.csv genesis_tx_inputs" 2>&1 | grep -v 'UNIQUE constraint failed' 1>&2
sqlite3 -csv crc20.sqlite ".import --skip 1 genesis_tx_outputs.csv genesis_tx_outputs" 2>&1 | grep -v 'UNIQUE constraint failed' 1>&2
