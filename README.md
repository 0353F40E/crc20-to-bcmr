# CRC-20 Tokenlist Generator

[Click here](bcmr/bitcoin-cash-metadata-registry.json) to go directly to Bitcoin Cash Metadata Registry (BCMR) tokenlist.
The BCMR specification can be found [here](https://github.com/bitjson/chip-bcmr).

## Methodology

The `update.sh` script does:

1. Execute [chaingraph query](program/crc20-genesis.graphml) to get transactions that:
   - Spend from the CRC-20 "Genesis" covenant contract pattern
   - Have the identity output of P2PKH locking script type
   - Have tokens on the output side.
2. Populate `genesis_tx_outputs` and `genesis_tx_inputs` tables. The CRC-20 redeem script is parsed in this step to extract the metadata and calculate the expected "marker" P2PKH bytecode.
3. Use the sqlite engine to execute [a SQL query](program/crc20.sql#L65) which:
   - Joins the two tables in such a way as to obtain a table of valid genesis candidates
   - Sorts the list by `fair_genesis_height`, `tx_hash`, `input_index` and produces a table of winners
5. Convert the result to BCMR .json. It uses the last checked block as registry's `latestRevision` and uses the timestamp of block which contains the token's genesis TX as the token's identity timestamp.

## Requirements

Linux with: `coreutils`, `jq`, `curl`, `gcc`, `sqlite3`

## Installation

Simply clone the repo, enter the `program` folder and run the `./make.sh` script.

```
git clone https://gitlab.com/0353F40E/crc20-to-bcmr.git
cd crc20-to-bcmr/program
./make.sh
```

## Usage

To stay in sync, simply run the `update.sh` from `program` folder at regular intervals.

```
./update.sh
```

## License

[MIT No Attribution](https://opensource.org/license/mit-0/).
