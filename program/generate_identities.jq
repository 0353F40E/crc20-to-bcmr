.[] as $tl |
{
  ($tl.token_category): {
    ($tl.timestamp): {
      "name":"[CRC20-\($tl.crc20_symbol)] \($tl.crc20_name)",
      "description": "CashTokens token category which first claimed this metadata according to CRC-20 genesis specification.\\nInitial fungible token supply: \($tl.initial_ft_supply)\\nGenesis TXID: \($tl.genesis_tx_hash).",
      "token": {
        "category": ($tl.token_category),
        "decimals": ($tl.crc20_decimals),
        "symbol": "\(if(($tl.crc20_symbol) | test("^[0-9A-Z][-0-9A-Z]+$")) then (("CRC20-")+($tl.crc20_symbol)) else (("CRC20X-")+($tl.crc20_symbol_hex)) end)"
      }
    }
  }
} 
