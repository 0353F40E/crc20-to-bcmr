# args: unlocking_bytecode
# output: csv of CRC-20 metadata
function get_crc20_info() {
    local symbol_length=$(echo "$1" | xxd -r -p | ./parse_pushes | tail -n 1 | cut -b 3- | xxd -r -p | ./parse_pushes -d | head -n 1)
    local pushes=$(echo "$1" | xxd -r -p | ./parse_pushes | tail -n 1 | cut -b 3- | xxd -r -p | ./parse_pushes | sed 's/^0x//g' )
    readarray <<<"$pushes" pushes
    local meta=$(./parse_crc20_metadata $symbol_length ${pushes[1]})
    readarray <<<"$meta" meta
    echo -n "${meta[@]}" | tr ' ' ',' | tr -d '\n'
    echo -n ",76a914$(echo "${meta[1]}" | xxd -r -p | ./hash160)88ac"
    echo -n ",${pushes[2]}" | tr -d '\n'
    echo -n ",$(echo "${meta[1]}" | xxd -r -p | strings -n 1)" | tr -d '\n'
    echo -n ",$(echo "${meta[3]}" | xxd -r -p | strings -n 1)" | tr -d '\n'
    echo
}
export -f get_crc20_info
