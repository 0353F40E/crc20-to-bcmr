#!/bin/bash

function generate_query()
{
  cat <<EOF
query {
  block(
    limit: 1
    offset: 0
    where: {
      accepted_by: { node: { name: { _like: "%mainnet" } } }
      height: { _eq: 0 }
    }
  ) {
    timestamp
  }
}
EOF
}

query=$(generate_query | jq -Rsa | sed "s/_eq: 0/_eq: $1/g")
query='{"query":'"$query"'}'

result=$(curl -s 'https://gql.chaingraph.pat.mn/v1/graphql' \
-H 'Accept-Encoding: gzip, deflate, br' \
-H 'Content-Type: application/json' \
-H 'Accept: application/json' \
-H 'Connection: keep-alive' \
-H 'DNT: 1' \
-H 'Origin: https://chaingraph.pat.mn' \
--data-binary "$query" \
--compressed)

echo "$result" | jq -r '.data.block[0].timestamp'
