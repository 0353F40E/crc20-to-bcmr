echo '{ "identities": {'
sqlite3 crc20.sqlite '.mode json' 'select * from crc20_tokenlist' \
| jq -f -r generate_identities.jq \
| sed 's/^{$//g' \
| sed 's/^}$/,/g' \
| head -n-1
echo '}}'
