source ./get_crc20_info.sh
test='4175400bb3fc748cbe82c2acf63e4f288a39fc0f7cc5f8eeb26c0e64aebf9b13c274a9ebba14f2535a186871fb23f43d2eaad70e2a5269a7f3df40550aa2e770aa614c66540d555344430455534420436f696e4104baab4239be98e023cb96ac71c156a0b14508bd1b1ca6f2911e7737097922a5b131ae1fe33f6804148d12a883f3d021c890573bd03e34260e268616804b42b0f0537a7cad7c7f75a90376a9147c7e0288ac7e00cd87'

symbol_length=$(echo "$test" | xxd -r -p | ./parse_pushes | tail -n 1 | cut -b 3- | xxd -r -p | ./parse_pushes -d | head -n 1)
pushes=$(echo "$test" | xxd -r -p | ./parse_pushes | tail -n 1 | cut -b 3- | xxd -r -p | ./parse_pushes | sed 's/^0x//g' )
readarray <<<"$pushes" pushes
printf "PUSH: %s" "${pushes[@]}"
echo CRC20 INFO TABLE
get_crc20_info "$test"
