BEGIN TRANSACTION;
CREATE TABLE IF NOT EXISTS "genesis_tx_inputs" (
	"tx_block_height"	INTEGER,
	"tx_block_timestamp"	INTEGER,
	"tx_hash"	TEXT,
	"tx_version"	INTEGER,
	"tx_input_count"	INTEGER,
	"op_block_height"	INTEGER,
	"op_block_timestamp"	INTEGER,
	"op_value_satoshis"	INTEGER,
	"op_token_category"	TEXT,
	"op_nft_capability"	INTEGER,
	"op_nft_commitment"	TEXT,
	"op_ft_amount"	INTEGER,
	"op_locking_bytecode"	TEXT,
	"input_index"	INTEGER,
	"transaction_hash"	TEXT,
	"outpoint_index"	INTEGER,
	"unlocking_bytecode"	TEXT,
	"sequence_number"	INTEGER,
	"output_count"	INTEGER,
	"tx_locktime"	INTEGER,
	"crc20_symbol_length"	INTEGER,
	"crc20_symbol_hex"	TEXT,
	"crc20_decimals"	INTEGER,
	"crc20_name_hex"	TEXT,
	"crc20_marker_p2pkh"	TEXT,
	"crc20_creator_key"	TEXT,
	"crc20_symbol"	TEXT,
	"crc20_name"	TEXT,
	PRIMARY KEY("tx_hash","input_index")
);
CREATE TABLE IF NOT EXISTS "genesis_tx_outputs" (
	"tx_hash"	TEXT,
	"output_index"	INTEGER,
	"value_satoshis"	INTEGER,
	"token_category"	TEXT,
	"nft_capability"	INTEGER,
	"nft_commitment"	TEXT,
	"ft_amount"	INTEGER,
	"locking_bytecode"	TEXT,
	PRIMARY KEY("tx_hash","output_index")
);
CREATE INDEX IF NOT EXISTS "i_marker_p2pkh" ON "genesis_tx_inputs" (
	"crc20_marker_p2pkh"
);
CREATE INDEX IF NOT EXISTS "o_marker_p2pkh" ON "genesis_tx_outputs" (
	"locking_bytecode"
);
CREATE INDEX IF NOT EXISTS "o_token_category" ON "genesis_tx_outputs" (
	"token_category"
);
CREATE INDEX IF NOT EXISTS "i_tx_hash" ON "genesis_tx_inputs" (
	"tx_hash"
);
CREATE INDEX IF NOT EXISTS "o_tx_hash" ON "genesis_tx_outputs" (
	"tx_hash"
);
CREATE INDEX IF NOT EXISTS "o_index" ON "genesis_tx_outputs" (
	"output_index"
);
CREATE INDEX IF NOT EXISTS "i_index" ON "genesis_tx_inputs" (
	"input_index"
);
CREATE VIEW "crc20_tokenlist" AS 
WITH genesis_candidates AS (
  SELECT 
    Row_number() over(
      PARTITION BY crc20_marker_p2pkh 
      ORDER BY 
        Max(
          genesis_tx_inputs_has_marker.op_block_height, 
          genesis_tx_inputs_has_marker.tx_block_height - 20
        ), 
        genesis_tx_inputs_has_marker.tx_hash, 
        genesis_tx_inputs_has_marker.input_index
    ) AS nr, 
    Max(
      genesis_tx_inputs_has_marker.op_block_height, 
      genesis_tx_inputs_has_marker.tx_block_height - 20
    ) AS fair_genesis_height, 
    genesis_tx_inputs_has_marker.*, 
    genesis_tx_outputs.* 
  FROM 
    (
      SELECT 
        * 
      FROM 
        (
          SELECT 
            genesis_tx_inputs.*
          FROM 
            genesis_tx_inputs 
        ) AS genesis_tx_inputs 
        inner join (
          SELECT 
            tx_hash, locking_bytecode
          FROM 
            genesis_tx_outputs 
          WHERE 
            genesis_tx_outputs.output_index == 0
        ) AS identity_outputs ON genesis_tx_inputs.tx_hash == identity_outputs.tx_hash 
        AND genesis_tx_inputs.crc20_marker_p2pkh == identity_outputs.locking_bytecode
    ) AS genesis_tx_inputs_has_marker 
    inner join (
      SELECT
        tx_hash,
        token_category,
        sum(ft_amount) as ft_supply,
        sum(iif(nft_capability == 'none',1,0)) as nft_immutable_count,
        sum(iif(nft_capability == 'mutable',1,0)) as nft_mutable_count,
        sum(iif(nft_capability == 'minting',1,0)) as nft_minting_count
      FROM
        genesis_tx_outputs
      WHERE
        token_category != ''
      GROUP BY tx_hash, token_category
    ) AS genesis_tx_outputs ON genesis_tx_inputs_has_marker.tx_hash == genesis_tx_outputs.tx_hash 
    AND genesis_tx_inputs_has_marker.transaction_hash == genesis_tx_outputs.token_category 
  ORDER BY 
    crc20_marker_p2pkh, 
    fair_genesis_height, 
    tx_hash, 
    input_index
) 
SELECT
  genesis_candidates.token_category,
  upper(genesis_candidates.crc20_symbol_hex) as crc20_symbol_hex,
  genesis_candidates.crc20_symbol,
  genesis_candidates.crc20_decimals,
  genesis_candidates.crc20_name,
  genesis_candidates.crc20_marker_p2pkh as symbol_utxo,
  cast(genesis_candidates.ft_supply as text) as initial_ft_supply,
  genesis_candidates.fair_genesis_height,
  genesis_candidates.tx_block_height as genesis_tx_height,
  genesis_candidates.tx_hash as genesis_tx_hash,
  strftime('%Y-%m-%dT%H:%M:%fZ', genesis_candidates.tx_block_timestamp, 'unixepoch', 'utc') as timestamp
FROM 
  genesis_candidates
WHERE 
  nr = 1
ORDER BY
  lower(crc20_symbol);
COMMIT;
