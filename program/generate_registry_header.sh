#!/bin/bash

function generate_registry_header()
{
  cat <<EOF
{
  "\$schema": "https://cashtokens.org/bcmr-v2.schema.json",
  "version": { "major": 1, "minor": 0, "patch": 0 },
  "latestRevision": "",
  "registryIdentity": {
    "name": "CRC-20 Token Registry",
    "description": "Automatically generated registry of tokens created according to CRC-20 specification.",
    "uris": {
      "icon": "https://ipfs.pat.mn/ipfs/QmUcyzmwyWqi7iP1PX7wtHGNozWFyughboorVSKTkWMdyY",
      "web": "https://ipfs.pat.mn/ipfs/QmYB7QLm7tyjqTawkV5GqRQPW2AAaZTsb2weCfmP6bP34Z",
      "registry": "https://gitlab.com/0353F40E/crc20-to-bcmr"
    }
  },
  "license": "CC0-1.0"
}
EOF
}

generate_registry_header | sed "s/\"latestRevision\": \"\"/\"latestRevision\": \"$1\"/g"
